/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import router from '@system.router';
import fileio from '@ohos.fileio';
import * as obvm from '../../common/runtime/vm.js';
import * as obcanvaslib from '../../common/canvas.js'
import prompt from '@system.prompt';

let filepath = '/data/openblock.xe';
export default {
    data: {
        info: null,
        vm: null,
        fsm: null,
        uninited: true,
        updateInterval: 0,
        fileCheckInterval: 0,
        filectime: -1,
        ui: [{
                 type: 'button'
             }]
    },
    ob_event(name) {
        if (this.fsm) {
            this.fsm.PostMessage(new obvm.OBEventMessage(name, null, null, this));
        }
    },
    initStage() {
        let stage = this.$refs.canvas1;
        try {
            stage.width = stage.width;
            let fd = fileio.openSync(filepath, 0o2);
            let st = fileio.fstatSync(fd);
            let scriptArrayBuffer = new ArrayBuffer(st.size);
            fileio.readSync(fd, scriptArrayBuffer);

            let obcanvas = new obcanvaslib.OBCanvas2D(stage);
            //            let ctx = stage.getContext('2d');
            //            ctx.fillRect(15, 15, 50, 50);

            let nativeLibs = [obcanvas.install.bind(obcanvas)];
            let loader = obvm.OBScriptLoader;
            let loadedScript = loader.loadScript(scriptArrayBuffer, nativeLibs);
            this.vm = new obvm.OBVM(loadedScript);
            //                // vm.Output = alert.bind(window);
            this.vm.Output = prompt.showToast;
            let fsmname = 'Start.Main';
            let fsm = this.vm.CreateFSM(fsmname);
            if (!fsm) {
                throw Error("No FSM named " + fsmname);
            }
            this.fsm = fsm;
        } catch (e) {
            this.info = "\nERROR:" + e.toString();
        } finally {
        }
    },
    onActive() {
        if (this.uninited) {
            this.uninited = false;
            this.initStage();
        }
        this.updateInterval = setInterval(this.update, 30);
        this.fileCheckInterval = setInterval(this.checkFile, 3000);
    },
    checkFile() {
        let fd = fileio.openSync(filepath, 0o2);
        let stat = fileio.fstatSync(fd);
        let ctime = stat.ctime;
        if (this.filectime == -1) {
            this.filectime = ctime;
        } else if (this.filectime != ctime) {
            this.filectime = ctime;
            this.initStage();
        }
    },
    onInactive() {
        clearInterval(this.updateInterval);
        clearInterval(this.fileCheckInterval);
    },
    update() {
        if (this.vm) {
            this.vm.update();
        }
    },
    onchange(type) {
        router.push({
            uri: "pages/" + type + "/index"
        })
    }
}
